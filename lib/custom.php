<?php
/**
 * Custom functions
 */

function google_fonts() {
	
?>
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,400italic' rel='stylesheet' type='text/css'>
<?php	

}
add_action('wp_head', 'google_fonts');
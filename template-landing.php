<?php
/*
Template Name: Landing Page
*/
?>
<style type="text/css">
	.bg-red {
		/*background: -webkit-radial-gradient(center, ellipse cover, #00BECE 0%,#C30000 100%);*/
		background: -webkit-radial-gradient(center, ellipse cover, #00BECE 0%,#504040 100%);
		color: whitesmoke;
		background: #00bece;
		background: -moz-radial-gradient(center, ellipse cover,  #00bece 0%, #504040 100%);
		background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%,#00bece), color-stop(100%,#504040));
		background: -webkit-radial-gradient(center, ellipse cover,  #00bece 0%,#504040 100%);
		background: -o-radial-gradient(center, ellipse cover,  #00bece 0%,#504040 100%);
		background: -ms-radial-gradient(center, ellipse cover,  #00bece 0%,#504040 100%);
		background: radial-gradient(ellipse at center,  #00bece 0%,#504040 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00bece', endColorstr='#504040',GradientType=1 );

	}
	.carousel-caption {
		color: black;
	}
</style>

<div class="row bg-red">
	<div class="container">
		<div class="row" style="margin-top:100px;">
		</div>
		<div class="col-lg-6">
			<!-- <img class="pull-right" style="height: 600px;" src="<?php echo get_template_directory_uri(); ?>/assets/img/single-drinks.png" alt="Invite people to unlock the deal for a free beer"> -->
			<img class="pull-right" style="height: 600px;" src="<?php echo get_template_directory_uri(); ?>/assets/img/activity-feed.png" alt="Coordinate a mountian biking trip with freinds and family">
		</div>

		<div class="col-lg-5">
			<h1>Koordit</h1>
            <h4>See the Fun. Be the Fun.</h4>
            
            <h3>Only <kbd>475</kbd> invites left</h3>
            <h4>Sign up to try out the private beta now!</h4>
            <br>   

            <!-- Begin MailChimp Signup Form -->
            <div id="mc_embed_signup">
              <form class="form-inline validate" action="http://koordit.us3.list-manage.com/subscribe/post?u=50cf2fdea58d040a3704df64e&amp;id=17b002f976&utm_source=Mailchimp&utm_medium=Sign%20Up&utm_term=Sign%20Up%20for%20Beta%20App&utm_content=Mailchimp%20Signup&utm_campaign=Mailchimp%20Sign%20Up" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_blank" novalidate>
                <div class="form-group mc-field-group">
                  <input class="form-control required email" type="email" value="" name="EMAIL" id="mce-EMAIL" placeholder="Enter email">
                </div>

                <div id="mce-responses" class="clear form-group">
                  <div class="response" id="mce-error-response" style="display:none"></div>
                  <div class="response" id="mce-success-response" style="display:none"></div>
                </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                <div style="position: absolute; left: -5000px;"><input type="text" name="b_50cf2fdea58d040a3704df64e_17b002f976" tabindex="-1" value=""></div>
                <div class="clear form-group">
                  <input class="button btn btn-success form-control" type="submit" value="Subscribe to the Beta App" name="subscribe" id="mc-embedded-subscribe">
                </div>
              </form>
            </div>
            <p>&nbsp;</p>
            <h3>Compete to Create the Most Fun!</h3>
            <p class="lead">It is a fundamental goal of Koordit to facilitate the sharing of ideas about fun with those around you. And then, of course, to help make that fun happen!</p>
			<!-- <a href="https://www.facebook.com/koordit?ref=br_tf">
				<img src="http://koordit.com/wp-content/uploads/2014/06/Facebook.png" style="padding-right:15px;"></a>
			<a href="http://www.youtube.com/watch?v=lmat__EGqM0">
				<img src="http://koordit.com/wp-content/uploads/2014/06/YouTube.png" class="social-icons social-footer"></a> -->
		</div>

		
	</div>
</div>

<div class="container">
	
	<!-- 3 Reasons to Join -->
	<div class="row row-centered" id="coordinate" style="padding:20px 0px;">  
		<div class="col-lg-10 col-centered">
	        <h3 class="text-center">Find and Create Fun</h3>       
	        <!-- <p class="lead text-center"><b>Who’s in?</b> Where we going? Who’s bringing what?  <em>Koordit</em> makes it <strong>easy</strong>. Its creative, quick, and most importantly, makes the fun happen!</p> -->
	        <p class="lead text-center">There is so much fun to be had around us. But making fun stuff happen with others can often be difficult to initiate and coordinate.</p>
	        &nbsp;
        </div>
  	</div>

  	<div class="row text-center">
        <div class="col-lg-4">
          <img class="img-circle" src="<?php echo get_template_directory_uri(); ?>/assets/img/one.png" width="150" alt="invite freinds to get and receive better deals">
          <h4>CREATE AND INVITE</h4>
          <p>Choose any type of activity you can think of, pick where, when, and with who you would like to do that activity with then let the fun begin!</p>
        </div><!-- /.col-lg-4 -->

        <div class="col-lg-4">
          <img class="img-circle" src="<?php echo get_template_directory_uri(); ?>/assets/img/three.png" width="150" alt="Unlock promotions and discounts">
          <h4>COMPETE & EARN</h4>
          <p>If your invitees come through and the activity is a success, then boom! Good times! Boom! Great deals!</p>
        </div><!-- /.col-lg-4 -->

        <div class="col-lg-4">
          <img class="img-circle" src="<?php echo get_template_directory_uri(); ?>/assets/img/two.png" width="150" alt="With enough people and freinds you can unlock deals">
          <h4>UNLOCK PROMOTIONS</h4>
          <p>Track your activity coordination prowess against others within your city! Who’s creating the most fun for the most people?</p>
        </div><!-- /.col-lg-4 -->

        
  	</div>

	<hr class="featurette-divider">

	<div class="row">
      <h3 class="text-center">WHAT DO THE PEOPLE THINK?</h3>
      <div class="col-md-12">
        <p class="lead text-center">We love feedback! It allows us to improve and refine our service everyday. Offer up some suggestions knowing that your ideas will help shape the finished product and contribute to the value and good times for thousands of others.  
      </div>
    </div>

	<div class="row">
		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">
		    <div class="item active">
		      <img class="img-circle aligncenter" src="<?php echo get_template_directory_uri(); ?>/assets/img/jenna.jpg" height="150" style="height: 170px !important;" alt="Koordit a app that coordinates freinds around activities">
		      <div class="carousel-caption"></div>
		      <div class="text-center">
		      	<h4 class="lead">Jenna M.</h4>
	     	 	<p>We've organized several neighborhood cleanups in Fairmount. Getting people <br>together was so easy and the results on the neighborhood's appearances have been incredible! </p>   
	     	  </div>
		    </div>
		    
		    <div class="item next">
		      <img class="img-circle aligncenter" src="<?php echo get_template_directory_uri(); ?>/assets/img/dukeh.png" height="150" style="height: 170px !important;" alt="Let's get everyone together for this basketball activity">
		      <div class="carousel-caption"></div>
		      <div class="text-center">
		      	<h4 class="lead">Duke H.</h4>
			    <p>We play basketball every Tuesday and I always know who's in and who's out.</p>
		      </div>
		    </div>

		    <div class="item next">
		      <img class="aligncenter" src="<?php echo get_template_directory_uri(); ?>/assets/img/timmy-circle.png" height="150" style="height: 170px !important;" alt="Getting freinds together to go golfing" >
		      <div class="carousel-caption"></div>
		      <div class="text-center">
				<h4 class="lead">Timmy C.</h4>
	      		<p>After a long day of accounting work in the office I just need to hit the links <br>for a couple hours. Koordit allows me to find the right golf partners to get <br>out there and enjoy the sunset and take that edge off.</p>   
		      </div>
		    </div>

		  </div>

		  <!-- Controls -->
		  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev" style="background-image:none;">
		    <span class="glyphicon glyphicon-chevron-left" style="color:black;"></span>
		  </a>

		  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next" style="background-image:none;">
		    <span class="glyphicon glyphicon-chevron-right" style="color:black;" ></span>
		  </a>
		</div>
	</div> <!-- end carousell row -->

	<hr class="featurette-divider">

	<!-- Begin MailChimp Signup Form -->
	<div class="row">
		<div class="text-center">
			<!-- <h4>Want More Fun? Well Then </h4> -->
		    <div id="mc_embed_signup">
		      <form class="form-inline validate" action="http://koordit.us3.list-manage.com/subscribe/post?u=50cf2fdea58d040a3704df64e&amp;id=17b002f976&utm_source=Mailchimp&utm_medium=Sign%20Up&utm_term=Sign%20Up%20for%20Beta%20App&utm_content=Mailchimp%20Signup&utm_campaign=Mailchimp%20Sign%20Up" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_blank" novalidate>
		        <div class="form-group mc-field-group">
		          <input class="form-control required email" type="email" value="" name="EMAIL" id="mce-EMAIL" placeholder="Enter email">
		        </div>
		        <div id="mce-responses" class="clear form-group">
		          <div class="response" id="mce-error-response" style="display:none"></div>
		          <div class="response" id="mce-success-response" style="display:none"></div>
		        </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
		        <div style="position: absolute; left: -5000px;"><input type="text" name="b_50cf2fdea58d040a3704df64e_17b002f976" tabindex="-1" value=""></div>
		        <div class="clear form-group">
		          <input class="button btn btn-success form-control" type="submit" value="Subscribe to the Beta App" name="subscribe" id="mc-embedded-subscribe">
		        </div>
		      </form>
		    </div>

		</div>
    </div>
    <div class="row">
	    <p>&nbsp;</p>
		<p>&nbsp;</p>
		<iframe width="560" height="315" class="center-block" src="//www.youtube-nocookie.com/embed/lmat__EGqM0?rel=0" frameborder="0" allowfullscreen>special</iframe>    
	    <p class="text-center">Special Thanks to <a href="https://www.facebook.com/principiaband">Principia</a> for their song <a href="https://www.youtube.com/watch?v=U_uR9LkWM7A">"Weekend Warriors"</a></p>
	    <p>&nbsp;</p>
	    <h3 class="text-center">The human mind, body, and soul needs fun and creativity.</h3>
	    <p class="lead text-center">Combine these forces for yourself and others with the beautifully useful Koordit!  </p>
    </div>



</div>
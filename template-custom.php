<?php
/*
Template Name: Custom Template
*/
?>




      <img class="full-width" src="<?php echo get_template_directory_uri(); ?>/assets/img/band.png" alt="An app for getting the group together and organizing them for band practice">  
      
      <div class="carousel-caption">
        <div class="row">

          <div class="col-lg-12 text-center">
            <h1>Coordinating Fun</h1>
            <h2>Introducing Your Local Activity App</h2>
            <h3>Only <strong>726</strong> invites left</h2>
            <h4>Sign up for the private beta now!</h4>
            <br>   

                       <!-- Begin MailChimp Signup Form -->
            <div id="mc_embed_signup">
              <form action="http://koordit.us3.list-manage.com/subscribe/post?u=50cf2fdea58d040a3704df64e&amp;id=17b002f976" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="form-inline validate" target="_blank" novalidate>
                <div class="mc-field-group">
                  <input type="email" value="" name="EMAIL" class="required email form-control" id="mce-EMAIL" placeholder="Enter email">
                </div>

                <div id="mce-responses" class="clear">
                  <div class="response" id="mce-error-response" style="display:none"></div>
                  <div class="response" id="mce-success-response" style="display:none"></div>
                </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                <div style="position: absolute; left: -5000px;"><input type="text" name="b_50cf2fdea58d040a3704df64e_17b002f976" tabindex="-1" value=""></div>
                <div class="clear">
                  <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button btn btn-primary btn-lg">
                </div>
              </form>
            </div>

          </div>
          

        </div>

        <div class="row">
          <a href="#coordinate"><span class="glyphicon glyphicon-arrow-down" style="font-size: 4.2em; padding-top:30px;"></span></a>
        </div>

      </div>

    </div>

	

<div class="container">

  <div class="row text-center" id="coordinate" style="padding:20px 0px;">  
        <h3 >Coordinate Your Group Activity!</h3>       
        <p class="lead">Fun times can be difficult to coordinate and manage. <b>Who’s in?</b> Who’s out? <br> 
        Who’s bringing what?  <em>Koordit</em> is here to <strong>help</strong>. It’s easy, fun, and get’s the job done!</p>
        &nbsp;
  </div>

  <div class="row text-center">
        <div class="col-lg-4">
          <img class="img-circle" src="<?php echo get_template_directory_uri(); ?>/assets/img/one.png" width="150" alt="invite freinds to get and receive better deals">
          <h4>CREATE AND INVITE</h4>
          <p>Choose any type of activity you can think of, pick where, when you would like to do that activity then let the fun begin!</p>
        </div><!-- /.col-lg-4 -->

        <div class="col-lg-4">
          <img class="img-circle" src="<?php echo get_template_directory_uri(); ?>/assets/img/two.png" width="150" alt="With enough people and freinds you can unlock deals">
          <h4>UNLOCK PROMOTIONS</h4>
          <p>Track your activity coordination prowess against others within your city! Who’s creating the most fun? <!-- Compete to be the best and unlock next-level venues, VIP treatment at the best spots in town, and even straight-up cash! --></p>
        </div><!-- /.col-lg-4 -->

        <div class="col-lg-4">
          <img class="img-circle" src="<?php echo get_template_directory_uri(); ?>/assets/img/three.png" width="150" alt="Unlock promotions and discounts">
          <h4>COMPETE & EARN</h4>
          <p>If your invitees come through and the activity is a success, then boom! Promotion! Boom! Discounts! Boom! Good times!</p>
        </div><!-- /.col-lg-4 -->
  </div>

  <hr class="featurette-divider">

  <div class="row featurette top-padding2">
    <div class="col-md-3">
      <img class="featurette-image img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/activity-feed.png" alt="Coordinate a mountian biking trip with freinds and family">
    </div>
    <div class="col-md-9">
      <h2 class="featurette-heading text-blue">Coordinating Fun is our Essential Mission <br><span class="text-muted">What Would You Coordinate?</span></h2>
      <p class="lead">Coordinate anything from a charitable event, pick-up soccer game, birthday party, poker tournament, chill sesh, paintball outing, or just a group of friends after work for a happy hour. </p>
      <br>
      <ul class="lead">
        <li>Finger scroll through any of the activities listed; or</li>
        <li>Create your own from scratch</li>
        <li>Invite anybody you know!</li>
      </ul>
      <br>
      <p class="lead">And the best part….if you do successfully coordinate some fun, not only do you get the baller status as the genius behind their activity happiness, you and the whole group will unlock special deals and promotions. </p>
    </div>   
  </div>

  <hr class="featurette-divider">

  <div class="row featurette top-padding2">
    <div class="col-md-9">
      <h2 class="featurette-heading text-blue">Unlock the Fun
      <span class="text-muted">That's all Around You</span></h2>  
      <p class="lead">Want an easy way to get some buddies together for the big game? Looking for the best way to get a group of your best girlfriends together for a day of shopping and fun in the city?
      <br>
      <br>
      Koordit delivers an easy-to-use platform plus the benefit of discounts on the things you know you’ll love to have when you’re all together. How bout a free pizza for getting those buddies together for the game or 10% off those silky smooth yoga pants for shopping with a group of your best friends?
      <br>
      <br>
      Koordit gives you both of these guaranteed instant benefits with the touch of a button- all in one easy to use app on your phone.  Just get your group together at a specific time and place and voila... your deal is unlocked! 
      <br>
      <br>
      The more people you get together the bigger your benefit or discount! Three people together for the Phillies game, get a free order of wings. Get 6 of your girlfriends in one spot for a personal yoga session or mani-pedi sesh and bingo, get the big toes painted for free!</p>
    </div>

    <div class="col-md-3">
      <img class="featurette-image img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/single-drinks.png" alt="Invite people to unlock the deal for a free beer">
    </div>      
  </div>

  <hr class="featurette-divider">
  
  <div class="row text-center">
    <br/>
    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-blue.png" style="padding-bottom:30px;"/>  
    <iframe width="560" height="315" class="center-block" src="//www.youtube-nocookie.com/embed/lmat__EGqM0?rel=0" frameborder="0" allowfullscreen></iframe>
    <hr class="featurette-divider">
    <div class="row text-center"><br/></div>
      <h3>WHAT DO THE PEOPLE THINK?</h3>
      <div class="col-md-12">
        <p class="lead">We love feedback! It allows us to improve and refine our service everyday. Offer up some suggestions knowing that your ideas will help shape the finished product and contribute to the value and good times for thousands of others.  
        <!-- Sign up below for the private beta and drop us a line<a href="mailto:info@koordit.com?subject=Feedback">contact</a> us. --></p>
      </div>
    </div>
    <br />
  </div>

<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active text-center">
      <img class="img-circle aligncenter" src="<?php echo get_template_directory_uri(); ?>/assets/img/jenna.jpg" height="150" style="height: 170px !important;" alt="Koordit a app that coordinates freinds around activities">
      <h4 class="lead">Jenna M.</h4>
      <p>We've organized several neighborhood cleanups in Fairmount. Getting people <br>together was so easy and the results on the neighborhood's appearances have been incredible! </p>   
    </div>

    <div class="item next text-center">
      <img class="aligncenter" src="<?php echo get_template_directory_uri(); ?>/assets/img/timmy-circle.png" height="150" style="height: 170px !important;" alt="Getting freinds together to go golfing" >
      <h4 class="lead">Timmy C.</h4>
      <p>After a long day of accounting work in the office I just need to hit the links <br>for a couple hours. Koordit allows me to find the right golf partners to get <br>out there and enjoy the sunset and take that edge off.</p>   
    </div>

    <div class="item text-center">
      <img class="img-circle aligncenter" src="<?php echo get_template_directory_uri(); ?>/assets/img/dukeh.png" height="150" style="height: 170px !important;" alt="Let's get everyone together for this basketball activity">
      <h4 class="lead">Duke H.</h4>
      <p>We play basketball every Tuesday and I always know who's in and who's out.</p>   
    </div>
  </div>
  


  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev" style="background-image:none;">
    <span class="glyphicon glyphicon-chevron-left" style="color:black;"></span>
  </a>

  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next" style="background-image:none;">
    <span class="glyphicon glyphicon-chevron-right" style="color:black;" ></span>
  </a>

</div>




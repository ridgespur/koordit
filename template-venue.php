<?php
/*
Template Name: Venue
*/
?>

<div class="row bg-blue text-white text-center">
	
	<div class="container">
		<div class="col-lg-3 venue-caption">
			<h1>Increase Local Foot Traffic</h1>
			<h2>Local <br> Social <br> Mobile</h2>
			<form class="form-inline" role="form">
              <div class="form-group">
                <label class="sr-only" for="exampleInputEmail2">Email address</label>
                <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Enter email">
              </div>
              <button type="submit" class="btn btn-default">Sign in</button>
            </form>   
		</div>
		<div class="col-lg-6">
			<img class="featurette-image img-responsive center-block" src="<?php echo get_template_directory_uri(); ?>/assets/img/single-drinks.png" alt="Venues can use our App to cordinate people around deals at their location brining in foot traffic">			
		</div>
		<div class="col-lg-3 venue-caption text-left">
			<p class="lead">Advantages of Marketing with Koordit</p>
			<ol>						
				<li>Effectively integrate and deliver the three most effective marketing strategies of the current and future business landscape: Mobile, Social, and Local</li>
				<li>Koordit’s Unique Proposition: Only Pay for What You Get</li>
			</ol>
		</div>	

	</div>


  	<!-- <div class="">
  		<h3>Coordinate Your Group Activity!</h3>       
	    <p class="lead">Fun times can be difficult to coordinate and manage. <b>Who’s in?</b> Who’s out? <br> 
	    Who’s bringing what?  <em>Koordit</em> is here to <strong>help</strong>. It’s easy, fun, and get’s the job done!</p>
  	</div> -->
	
</div>

<div class="container">

	<div class="row text-center">  
        <h2 class="lead">Pack More <b>Punch:</b> Advertising and Brand Awareness Efficiency</h2>
  	</div>

  	<div class="row">
  		<div class="col-lg-12 lead">
  			<p>Finally! Know what you are getting from your digital marketing. Koordit brings you into the future of digital marketing with it’s innovative Local, Social, Mobile platform that will provide you with the most cost efficient, effective and transparent results.</p>

			<p>And for now, under our Foundational Partners Program, all at a nearly next-to-nothing rate. Signing up as one of our first 50 marketing partners you will be able to lock in our-ultra low rate for your entire lifetime contract with Koordit. It’s a one in a lifetime opportunity to lock in the future of marketing with one of the most innovative companies in the fastest growing marketing space in the economy. </p>
  		</div>

  		<!-- <img class="col-lg-6" src="<?php echo get_template_directory_uri(); ?>/assets/img/mobile-online.png"> -->

  	</div>

  	<hr class="featurette-divider">

	<div class="row">
		<div class="row text-center">  
			<h2 class="lead">Get Them <b>In:</b> Who You Want and When You Want Them Pack More</h2>
	  	</div>
		
		<!-- <img class="col-lg-6" src="<?php echo get_template_directory_uri(); ?>/assets/img/mobile-online.png"> -->

		<div>
			<ul class="lead">
				<li>When our program sees that the participants did in fact come together, they are sent a promotion, discount, or free sample from a local business based on the consumer profile of the participants and when they are together. </li>
				<li>Our business partners can develop their own ideal consumer profile or use ours. User participants will be provided with in-app directions directly to your place of business</li>
				<li>The users will be shown the promotion before they have entered the place of business, however, it will not be activated and the business will not pay until they have physically arrived in the place of business</li>
			</ul>
  		</div>
	</div>

	<hr class="featurette-divider">
	
	<div class="row">
		<div class="col-lg-6">
			<h2>Let's Connect</h2>
			<p class="lead">Koordit is a business that helps other business get people in their door. We are a marketing company that aims to harness the three most effective areas of consumer responsiveness: mobile, local and social. Our business revolves around, and is facilitated through, a mobile application. The mobile application provides the premier platform for people to think of, initiate, and manage group activities of any kind. Using the information about who our users are and where and when they will be somewhere, we are able to deliver ultra-targeted ads from local business in their direct vicinity. </p>
		</div>

		<div class="col-lg-6">
			<?php echo do_shortcode( '[contact-form-7 id="12" title="Contact form 1"]' ); ?>
		</div>
	</div>

	<hr class="featurette-divider">
</div>








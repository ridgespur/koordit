<?php
/*
Template Name: About Us
*/
?>
<style type="text/css">

		body {
		   background-image: -webkit-linear-gradient(38deg, rgb(255, 255, 255), #B5F6FF 50%, #27E5FF 50%, rgb(199, 141, 141) 100%);
		   /*background-image: -webkit-linear-gradient(38deg, rgb(255, 255, 255), #B5F6FF 50%, #27E5FF 50%, rgb(199, 141, 141) 100%);
		   background-image: -moz-linear-gradient(38deg, rgb(255, 255, 255), #B5F6FF 50%, #27E5FF 50%, rgb(199, 141, 141) 100%);
		   background-image: -o-linear-gradient(38deg, rgb(255, 255, 255), #B5F6FF 50%, #27E5FF 50%, rgb(199, 141, 141) 100%);
		   background-image: linear-gradient(38deg, rgb(255, 255, 255), #B5F6FF 50%, #27E5FF 50%, rgb(199, 141, 141) 100%);*/
		}

		body {
			background: #1e5799;
			background: -moz-linear-gradient(top, #1e5799 0%, #207cca 41%, #2989d8 50%, #7db9e8 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#1e5799), color-stop(41%,#207cca), color-stop(50%,#2989d8), color-stop(100%,#7db9e8));
			background: -webkit-linear-gradient(top, #1e5799 0%,#207cca 41%,#2989d8 50%,#7db9e8 100%);
			background: -o-linear-gradient(top, #1e5799 0%,#207cca 41%,#2989d8 50%,#7db9e8 100%);
			background: -ms-linear-gradient(top, #1e5799 0%,#207cca 41%,#2989d8 50%,#7db9e8 100%);
			background: linear-gradient(to bottom, #1e5799 0%,#207cca 41%,#2989d8 50%,#2F7FBE 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1e5799', endColorstr='#7db9e8',GradientType=0 );
		}

		.container a {
			color: black;
			text-transform: bold;
			text-decoration: underline;
		}
	
</style>

<div class="container lead text-white">
	<div class="row">
		<div style="padding-top:100px;"></div>
		<img class="center-block" style="width: 20%;" src="<?php echo get_template_directory_uri(); ?>/assets/img/app-logo.png" alt="Koordit app logo - Coordinating Activites and Fun"/>
		<h1 class="text-center">About Us <br> Organize Social Events and Activities</h1>
		<p>Welcome to Koordit! We are excited and grateful to have you here on our launch pad and we truly feel you are fortunate to have found us! Here at Koordit we are dedicated to helping you easily bring more fun and positive energy into your life. We believe that fun and engaging social action with other’s will dramatically improve the success you have in life, particularly your motivation, inspiration and of course the ultimate success in life, peace and happiness.  Heck, you might even consider having a world full of fun as purpose enough in life!</p>
		<p>Our fundamental mission and focus everyday at Koordit is to create the best easy-to-use service that allows people to harvest more fun in their life from all of the people, places and resources around you. The potential for fun and the positive energy that it creates is all around us!</p>
	</div>
	&nbsp;
	<hr class="featurette-divider">
	<div class="row">
		<img class="img-circle col-lg-2" src="<?php echo get_template_directory_uri(); ?>/assets/img/jonny.jpg" alt="Cofounder Jon Salem Koordit Coordinating Fun">
		<div class="col-lg-10">
			<p>Founder Jonathan Salem was born and raised on the outskirts of Philadelphia, where he attended the great Rydal Elementary school. After some high school learning and fun, Jonathan attended Hopkins in Bmore, majoring in Economics. While there and learning a little bit from his teachers and a lot from his fellow students, he grew his passion for economics, fun, ideas, and coordinating resources. After graduating, Jonathan entered the consulting and financial services business. After a total of 3.5 years with the company that he started his career with, he decided to resign his position to pursue his dream to build a sustainable organization that produces beautiful, enduring value in creative ways. That organization is Koordit! And now… We can’t be more excited to be at your service to produce the best possible value for you in the most convenient way everyday!</p>
		</div>
	</div>
	<hr class="featurette-divider">
	<div class="row">
		<div class="col-lg-10">
			<p>Josh Meth moved to Philadelphia to pursue his college career.  Unbeknownst to him he received a full education working at many odd jobs before entering into the business world where he played a Digital Marketing role at <a href="http://www.phillymarketinglabs.com/">Philadelphia Marketing Labs</a>, Sales at <a href="http://www.fluxnflow.com">Flux&Flow</a>, Web Development at <a href="http://www.referable.com">Referable</a>, Web App Development at <a href="http://www.competeleap.com">CompeteLeap</a> and Project Management at Koordit.  </p>

			<p>Josh enjoys his days working in different spaces, playing soccer and having long dinners with friends and family.  Psychology, Business, Technology and Design (in that order) and has played a major role in Josh’s life and is expressed in his projects.  </p>
		</div>
		
		<img class="img-circle col-lg-2" src="<?php echo get_template_directory_uri(); ?>/assets/img/josh.jpg" alt="Cofounder Josh Meth Koordit Coordinating Fun">
	</div>

	<hr class="featurette-divider">
</div>
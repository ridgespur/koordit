<footer class="content-info" role="contentinfo">
  <div class="container">
 	<hr class="featurette-divider">
    <?php dynamic_sidebar('sidebar-footer'); ?>
    <p><?php echo date('Y'); ?> - <?php bloginfo('name'); ?></p>
  </div>
</footer>

<?php wp_footer(); ?>
